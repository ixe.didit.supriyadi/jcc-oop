<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php'); 

$sheep = new animal("shaun");
echo "name :" . $sheep ->type;
echo "<br>"; 
echo "legs :" . $sheep ->legs;
echo "<br>"; 
echo "Cold Bloded :" . $sheep ->cold_blooded;
echo "<br>";
echo "<br>";

$katak = new frog ("Buduk");
echo "name :" . $katak ->type;
echo "<br>"; 
echo "legs :" . $katak ->legs;
echo "<br>"; 
echo "Cold Bloded :" . $katak ->cold_blooded;
echo "<br>"; 
echo "Jump :" . $katak ->jump;
echo "<br>";
echo "<br>";

$monkey = new ape ("Kera Sakti");
echo "name :" . $monkey ->type;
echo "<br>"; 
echo "legs :" . $monkey ->legs;
echo "<br>"; 
echo "Cold Bloded :" . $monkey ->cold_blooded;
echo "<br>"; 
echo "Jump :" . $monkey ->yell;
echo "<br>";
?>